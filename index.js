require('dotenv').config()


const { GraphQLClient, gql } = require('graphql-request');

const endpoint = 'https://api-dev.speak.ef.com/graphql';
const token = process.env.TOKEN;
const graphQLClient = new GraphQLClient(endpoint, {
    headers: {
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
    },
});



async function getCourse(courseId, parentLevel=2, subLevel=3) {

    const queryCollection = `
        id
        name
        description
        collectionCount
        activityCount
        backgroundAsset {
            id
            url
        }
        team {
            id
            avatarAsset {
                id
                name
                url
                sizeBytes
                mime
                tags {
                    id
                    name
                    color
                }
            }
        }
        activities {
            id
            name
            publishedRevision {
                id
                rawData
            }
        }
        tags {
            id
            name
        }
    `;

    let parent = '';
    for (let plevel=0; plevel<parentLevel; plevel++){
        parent = `parent {
            ${queryCollection}
            ${parent}
        }`
    }

    let sub = '';
    for (let slevel=0; slevel<subLevel; slevel++){
        sub = `subCollections {
            ${queryCollection}
            ${sub}
        }`
    }

    const queryBody = `
        ${queryCollection}
        ${parent}
        ${sub}
    `;

    const activityCollectionByIdQuery = gql`query ActivityCollection($id: String) {
        activityCollection(id: $id) {
          ${queryBody}
        }
      }`;
    
    const variables = {
        id: `${courseId}`
    }


    const data = await graphQLClient.request(activityCollectionByIdQuery, variables);
    
    function parseRawData(obj){
        if (obj.subCollections){
            for (let s of obj.subCollections){
                parseRawData(s)
            }
        }
        
        if (obj.activities){
            for (let a of obj.activities){
                a.publishedRevision.rawData = JSON.parse(a.publishedRevision.rawData);
            }
            
        }
        
    }

    const result = data.activityCollection
    parseRawData(result)
    console.log(JSON.stringify(result, undefined, 2))

}

const courseId = process.argv[2];
console.log(`fetching course ${courseId}`)
getCourse(courseId, 5, 5).catch((error) => console.error(error));