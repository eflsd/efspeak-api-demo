require('dotenv').config()


const { GraphQLClient, gql } = require('graphql-request');

const endpoint = 'https://api.speak.ef.com/graphql';
const token = process.env.TOKEN;
const graphQLClient = new GraphQLClient(endpoint, {
    headers: {
        authorization: `Bearer ${token}`,
        'content-type': 'application/json'
    },
});



async function getStat(teamId, activityIds) {
    const act = gql`query activity ($id: String!){
        activityByID (id: $id) {
            id
            name
        }
    }`;

    const statistics = gql`query statistics ($teamId: String!, $filters: ResultStatisticFiltersInput!){
        resultStatistics (teamId: $teamId, filters: $filters) {
          count
          avgScore
        }
    }`;
    
    const ret = [];
    for (let aid of activityIds) {
        const data = await graphQLClient.request(statistics, {
            teamId, 
            filters: {
                activityId: aid
            }
        });
        
        const aData = await graphQLClient.request(act, {
            id: aid
        });
        ret.push({
            stat: data.resultStatistics,
            activity: aData.activityByID
        });
    }
    return ret;
}

const teamId = process.argv[2];
const aids = process.argv[3].split(',');

getStat(teamId, aids).then(ret => {
    console.log(`id,name,avgScore,count`)
    for (const r of ret){
        console.log(`${r.activity.id},${r.activity.name},${r.stat.avgScore},${r.stat.count}`)
    }
}).catch((error) => console.error(error));