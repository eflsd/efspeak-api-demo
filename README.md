# How to use
1. Obtain JWT token from the web interface
2. Copy paste it and put it in .env (see .env.sample sample)
3. Get the collection ID (course) from the URL of the editing interface. E.g. https://dev.speak.ef.com/dashboard/collection/107/details, the id is 107
3. run `index.js {id}`. E.g. `node index.js 107`